from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import TechnicianEncoder, AppointmentListEncoder, AppointmentDetailEncoder
from .models import AutomobileVO, Technician, Appointment 
import json
import logging

logged = logging.getLogger(__name__)

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )



@require_http_methods(["GET", "DELETE"])
def api_detail_technician(request, id):
    if request.method == "GET":
        technicians = Technician.objects.get(employee_id=id)
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        count, _ = Technician.objects.filter(employee_id=id).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        print(appointments)
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentListEncoder, safe=False
        )
    else: # POST
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
            if AutomobileVO.objects.filter(vin=content["vin"]).exists():
                content["vip"] = True
        except Exception as e:
            return JsonResponse(
                {"message": f"Invalid Technician: {str(e)}"},
                status=400,
            )
        appointments = Appointment.objects.create(**content)
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_appointment(request, pk):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointments,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404
                )

    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointments = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET"])
def api_service_history(request, vin):
    appointments = Appointment.objects.filter(vin=vin)
    if appointments:
        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        return JsonResponse(
            {"message": "Service history does not exist"},
            status=404
        )
