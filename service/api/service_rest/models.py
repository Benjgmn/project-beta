from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

    class Meta:
        ordering = ("vin", "sold")


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.first_name


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.TextField(max_length=300)
    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.PROTECT, null=True
    )

    def __str__(self):
        return f"{self.customer}'s appointment on {self.date} "

    def get_api_url(self):
        return reverse("api_detail_appointment", kwargs={"id": self.id})