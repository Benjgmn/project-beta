from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "address",
        "id",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "salesperson",
        "customer",
        "vin",
    ]
    encoders = {"vin": AutomobileVOEncoder(),
                "customer": CustomerEncoder(),
                "salesperson": SalespersonEncoder()}
