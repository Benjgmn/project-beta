# CarCar

Team:

* Ben Weiss - Sales
* Mason Bills - Services

## Design

## Service microservice

The models that have been created for this project include an appointments model, a technician model and a automobileVO all of which create a data table in the database that contains properties you would expect to be associated with an appointment and a technician. If youve gone over to the models.py file and taken a look at the automobileVO model you might ask why it is so sparce in associated properties? Shouldn't an automobile have more characteristics? The short answer is the AutomoibileVO is a special model in the sense that it allows the Services microservice to send and recieve data from the inventory microservice's Automobile. The reason for the AutomobileVO only having a few properties is to only recieve necessary data; it essentially creates a point of reference to the Auotmobile model that is found in the inventory microservice. This point of refernce is created via a poller system. The poller reaches out to the inventory api end point to update any data specified in the poller code function. 

## Sales microservice

Through the sales microservice, I defined 4 models (Salesperson, Customer, Sale, and AutomobileVO) that when working together with encoders and view functions can create, read, and delete data from the database. Of these models, AutomobileVO interacts with a poller in order to get access the "vin" and "sold" fields from the automobile object inside of Inventory. Next, I created a React frontend that can perform all the same operations. As for how this uniquely interacts with Inventory, I made it so that my CreateSale component would send a PUT request to update the sold status of the automobile that is being sold upon form submission.
