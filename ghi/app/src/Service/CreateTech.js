import React, { useState } from 'react';

function CreateTechnician() {
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    employeeId: '',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    const newTechnician = {
      first_name: formData.firstName,
      last_name: formData.lastName,
      employee_id: formData.employeeId,
    };

    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newTechnician),
      });

      if (!response.ok) {
        throw new Error('Response not ok');
      }

      const newTech = await response.json();
      console.log(newTech);
      alert("Technician created successfully!");

      setFormData({
        firstName: '',
        lastName: '',
        employeeId: '',
      });
    } catch (error) {
      alert("There was an error creating the technician");
    }
  };

  const handleFormChange = (e) => {
    const inputName = e.target.name;
    const value = e.target.value;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  }

  return (
    <div className="container">
      <div className='shadow-lg p-3 mb-5 mt-5 bg-white rounded w-50 mx-auto'>
      <h1 className="my-4">Create New Technician</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label className="form-label">First Name:</label>
          <input type="text" name="firstName" className="form-control" value={formData.firstName} onChange={handleFormChange} />
        </div>
        <div className="mb-3">
          <label className="form-label">Last Name:</label>
          <input type="text" name="lastName" className="form-control" value={formData.lastName} onChange={handleFormChange} />
        </div>
        <div className="mb-3">
          <label className="form-label">Employee ID:</label>
          <input type="text" name="employeeId" className="form-control" value={formData.employeeId} onChange={handleFormChange} />
        </div>
        <button type="submit" className="btn btn-primary">Create Technician</button>
      </form>
      </div>
    </div>
  );
}

export default CreateTechnician;
