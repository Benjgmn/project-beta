import React, { useState, useEffect } from 'react';

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState('');


    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        } else {
            console.error("Response not ok");
        }
    }
    useEffect(() => {
        getData();
    }, []);

    function handleClickSearch()
        {if (search === '') { 
            getData(); 
            return;

    }   const filteredAppointments = appointments.filter(appointment => {
            return appointment.vin.toLowerCase().includes(search.toLowerCase());
        })
        setAppointments(filteredAppointments);
    }
    function handleKeyPress(e) {
        if (e.key === 'Enter') {
            handleClickSearch();
        }
    }

    return (
        <div className="container">
            <h1 className="my-4">Service History</h1>
            <input type="text" onKeyDown={handleKeyPress} onChange={e => setSearch(e.target.value)} placeholder="Enter VIN" />
            <button className='btn btn-primary' style={{ margin: '10px' }} onClick={handleClickSearch}>Search</button>
            <table className="table">
                <thead>
                    <tr>
                        <th>VIN Number</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Technician</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment, index) => (
                        <tr key={index}>
                            <td>{appointment.vin}</td>
                            <td style={{ color: 'red' }}>{appointment.vip ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.technician.first_name}</td>
                            <td>{appointment.finished ? "Finished" : appointment.cancelled ? "Cancelled" : "Created"}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            
        </div>
    );
}

export default ServiceHistory;
