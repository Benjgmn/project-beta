import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


function ListAppointments() {
  const [appointments, setAppointments] = useState([]);

  const getData = async () => {
      const response = await fetch('http://localhost:8080/api/appointments/');
    
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
      } else {
        console.error("Response not ok");
      }
  }
  useEffect(() => {
    getData();
  }, []);



  const cancelAppointment = async (id) => {

    const deleteFunctionUrl = `http://localhost:8080/api/appointments/${id}/`
    const options = {
        method: 'PUT',
        body: JSON.stringify({
            cancelled: true
        }),
        headers: {
            "Content-Type": "application/json"
        }

    }
    const response = await fetch(deleteFunctionUrl, options)
    if (response.ok) {
        window.location.reload()
    }
}

const finishAppointment = async (id) => {

    const finishFunctionUrl = `http://localhost:8080/api/appointments/${id}/`
    const options = {
        method: 'PUT',
        body: JSON.stringify({ finished: true }),
        headers: { 'Content-Type': 'application/json' }

    }
    console.log(finishFunctionUrl)
    const response = await fetch(finishFunctionUrl, options)
    if (response.ok) {
        window.location.reload()
    }
}



  return (
    <div className="container">
      <h1 className="my-4">Appointments List</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN Number</th>
            <th>VIP</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Reason</th>
            <th>Technician</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment, index) => (
            <tr key={index}>
              <td>{appointment.vin}</td>
              <td style={{ color: 'red' }}>{appointment.vip ? "Yes" : "No"}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date}</td>
              <td>{appointment.time}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.technician.first_name}</td>
              <td>{appointment.finished ? <p>Complete!</p> : (appointment.cancelled ? <p>Cancelled!</p> : null)}</td>
              <>
              <td>{!appointment.finished && !appointment.cancelled && 
                <button className="btn btn-success"
                    onClick={() => finishAppointment(appointment.id)}
                >Finish</button>
                }</td>
              <td>{!appointment.finished && !appointment.cancelled && 
                <button className="btn btn-danger"
                    onClick={() => cancelAppointment(appointment.id)}
                >Cancel</button>
                }</td>
                </>
            </tr>
          ))}
        </tbody>
      </table>
      <Link to="/appointments/create" className="btn btn-primary">Add Appointment</Link>
    </div>
  );
}

export default ListAppointments;
