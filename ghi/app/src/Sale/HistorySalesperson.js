import React, { useState, useEffect } from "react";

function HistorySalesperson() {
  const [salesperson, setSalesperson] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");
  const [sale, setSale] = useState([]);

  const fetchData = async () => {
    const salespersonUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salespersonUrl);
    if (response.ok) {
      const salespersonContent = await response.json();
      setSalesperson(salespersonContent.Salespeople);
    }

    const saleUrl = "http://localhost:8090/api/sales/";
    const saleInfoResponse = await fetch(saleUrl);
    if (saleInfoResponse.ok) {
      const saleContent = await saleInfoResponse.json();
      setSale(saleContent.sales);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const saleTable = (sales) => {
    let tableData = [];
    for (const sale of sales){
      if (sale.salesperson.employee_id === selectedSalesperson){
        tableData.push(
          <tr key={sale.id}>
            <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
            <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
            <td>{sale.vin.vin}</td>
            <td>{"$" + sale.price}</td>
          </tr>
        )
      }
    }
    return tableData;
  }
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Salesperson History</h1>
            <div className="form-floating mb-3">
                <select
                  className="form-select"
                  value={selectedSalesperson}
                  onChange={(event) => setSelectedSalesperson(event.target.value)}
                >
                  <option value="">Select Salesperson</option>
                  {salesperson?.map((person, index) => (
                    <option key={index} value={person.id}>
                      {person.first_name + " " + person.last_name}
                    </option>
                  ))}
                </select>
              </div>
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  {saleTable(sale)}
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HistorySalesperson;
