import { useEffect, useState } from "react";
import {Link} from 'react-router-dom'

function ListSalesperson() {
  const [salespeople, setSalespeople] = useState([]);

  async function fetchData() {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
      const content = await response.json();
      setSalespeople(content.Salespeople);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
  return(
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople?.map((person, index) => (
            <tr key={index}>
              <td>{person.employee_id}</td>
              <td>{person.first_name}</td>
              <td>{person.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <Link to="/salespeople/create" className='btn btn-primary'>Create Salesperson</Link>
    </>
  );
}

export default ListSalesperson;
