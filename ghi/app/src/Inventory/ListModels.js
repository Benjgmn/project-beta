import React, { useState, useEffect } from 'react';

function ListModels() {
    const [models, setModels] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
console.log(response)
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
            console.log(data)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    return (
<>
        <div className="container">
            <h2>Model List</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Picture</th>
              <th>Manufacturer</th>
            </tr>
          </thead>
          <tbody>
            {models.map( model => {
              return (
                <tr key={model.href}>
                  <td>{ model.name }</td>
                  <td>
                    <img style={{ width: '200px' }} src={ model.picture_url }/>
                    </td>
                  <td>{ model.manufacturer.name }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
        </>
    );
  }

export default ListModels;