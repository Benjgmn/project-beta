import React, { useState, useEffect } from "react";

function CreateAuto() {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        color: "",
        vin: "",
        year: "", 
        model_id: "",

    })
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
            console.log(data)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const automibileUrl = 'http://localhost:8100/api/automobiles/';
            
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(automibileUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                color: '',
                vin: "",
                year: "",
                model_id: "",
            })
            alert("Automobile created successfully!");
        }
    }
    const handleChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
        console.log(formData)
    }

    

    return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Automobile Form</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <input onChange={handleChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
            <label htmlFor="color">Color</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
            <label htmlFor="vin">VIN</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChange} value={formData.year} placeholder="year" required type="text" name="year" id="year" className="form-control" />
            <label htmlFor="year">Year</label>
          </div>
          <div className="mb-3">
              <select onChange={handleChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                <option value="">Choose a automobile</option>
                {models.map( model => {
                  return (
                    <option key={model.id} value={model.id}>{model.name}</option>
                  )
                })}
              </select>
            </div>
            <button type="submit" className="btn btn-primary">
            Add Automobile
          </button>
        </form>
      </div>
    </div>
  </div>
  )
}


export default CreateAuto