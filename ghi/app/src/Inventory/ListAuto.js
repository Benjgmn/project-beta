import React, { useState, useEffect } from 'react';

function ListAuto() {
    const [autos, setAutos] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
console.log(response)
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
            console.log(data)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    return (
<>
        <div className="container">
            <h2>Automobile List</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Model</th>
              <th>Year</th>
              <th>VIN</th>
              <th>Color</th>
            </tr>
          </thead>
          <tbody>
            {autos.map( car => {
              return (
                <tr key={car.vin}>
                  <td>{ car.model.name }</td>
                  <td>{ car.year }</td>
                  <td>{ car.vin }</td>
                  <td>{ car.color }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
        </>
    );
  }

export default ListAuto;